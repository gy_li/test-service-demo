package com.fsnip;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by 46163 on 2018/3/27.
 */
@FeignClient(name= "gateway-server")
public interface HelloRemote {

    @RequestMapping(value = "/hello")
    public String hello(@RequestParam(value = "name") String name);

    @RequestMapping(value = "/hello1")
    public String hello1(@RequestParam(value = "name") String name);
}
