package com.fsnip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fsnip.gate.agent.rest.ApiGateSecurity;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
@EnableCircuitBreaker
@RestController
@ApiGateSecurity
public class ProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}

	@Autowired
	DiscoveryClient discoveryClient;

	@RequestMapping("/hello")
	public String index(@RequestParam String name) {
		return "hello "+name+"，this is first messge";
	}

	@RequestMapping("/jazhen")
	public String index11(@RequestParam String name) {
		return "hello "+name+"，this is first messge";
	}

	@RequestMapping("/jazhen/hello")
	public String index121(@RequestParam String name) {
		return "hello "+name+"，this is first messge";
	}

	@RequestMapping("/hello1")
	public String index1() {
		return "server: "+discoveryClient.getServices();
	}


}
