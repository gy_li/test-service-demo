package org.crazyit.test.ctl;

import org.crazyit.test.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@Autowired
	private HelloService helloService;
	
	@RequestMapping(method = RequestMethod.GET, value = "/hello")
	public String auth(String username) {
		return helloService.hello(username);
	}

}
