package org.crazyit.test.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("gateway-server")
public interface HelloService {
	
	@RequestMapping(method = RequestMethod.GET, value = "/hello")
	public String hello(@RequestParam(value = "name") String name);

}
